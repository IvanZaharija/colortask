import React from 'react';
import './App.css';
import TaskOne from './components/TaskOne'
import TaskTwo from './components/TaskTwo';

function App() {
  return (
    <div className="App">
        <TaskOne />
        <TaskTwo />
    </div>
  );
}

export default App;
