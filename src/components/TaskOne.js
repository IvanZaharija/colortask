import React, {Component} from 'react';

 class TaskOne extends Component {
     constructor() {
         super()    
     }

     state = {
         colorList : [],
         color : ''
     }

     addColor = () => {

        fetch('http://www.colr.org/json/color/random')
        .then(res => res.json())
        .then((data) => {
            this.setState( { color : data.new_color});
            this.addToList();
        })
     }

     addToList = () => {
    
        var addColorList = this.state.colorList;
        var newColor = this.state.color

        addColorList.push(newColor);

        this.setState( {colorList: addColorList})

     }

     render () {

        console.log(this.state.color);
        console.log("list is" + this.state.colorList);

        var showList = this.state.colorList.map ((item) => 
                <li key={item + Math.random()} style={{color: "#" + item }}>{item}</li>
            )

         return (

        <div className="task-one-container">
            <div className="button-continer" >
                <button onClick={this.addColor} >Press me for a color</button>
            </div>
            <div className="color-list">
                <p>Color List</p>
                <ul>
                    {showList}
                </ul>
            </div>
            

        </div>
         )
     }
 }

 export default TaskOne
