import React, {Component} from 'react';

 class InputComponent extends Component {
     constructor(props) {
         super(props)    

         this.state = {
        }
     }

     render () {

        return(

            <div >
                <input 
                    type="text"
                    onChange={this.props.returnChangedText}
                    value={this.props.text}
                />
            </div>
        )
     }
     
 }

 export default InputComponent
