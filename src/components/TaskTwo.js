import React, {Component} from 'react';
import InputComponent from './InputComponent';

 class TaskTwo extends Component {
     constructor() {
         super();
         this.state = {
            inputText: 'This text is chaning'
        }
     }

     changeText = (event) => {

        this.setState( {inputText: event.target.value})
     }


     render () {
        return(

            <div className="task-two-container">
                <div>
                    <h1>Text to show</h1>
                    <p>{this.state.inputText}</p>
                </div>

            <InputComponent 
                returnChangedText={this.changeText}
                text={this.state.inputText}
                />
    
            </div>
        )
     }  
 }

 export default TaskTwo
